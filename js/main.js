var staffList = [];
document.getElementById('btnThem').addEventListener('click', function() {
    document.getElementById('btnThemNV').removeAttribute('data-dismiss','modal')
})
function themNV() {
    //lấy nhân viên từ form
    var newStaff = createStaff();
    

    // check id
    let isValid = required(newStaff.staffId, "tbTKNV")
        && kiemTraDoDai(newStaff.staffId, 'tbTKNV', 4, 6)
        && checkNumberId(newStaff.staffId)
        && kiemTraTrung(newStaff.staffId, staffList)
    let isValidform = validateForm(newStaff);

    if(isValid &= isValidform) {
            //push NV vào DSNV
            staffList.push(newStaff);
            //lưu  danh sách vào localStorage
            setStaffList();
            // in nhân viên ra bảng
            renderStaff(staffList);
            // clear form
            document.getElementById('staffForm').reset();
            // close form
            document.getElementById('btnThemNV').setAttribute('data-dismiss','modal')
    }
}


function setStaffList() {
    var dataJson = JSON.stringify(staffList);
    localStorage.setItem('LOCAL_STAFF', dataJson);
}
function getStaffList() {
    var dataJson = localStorage.getItem('LOCAL_STAFF');
    if(dataJson != null){
        staffList = mapData(JSON.parse(dataJson));
        renderStaff(staffList);
    }
}
getStaffList();

function mapData(dataJson) {
    var result = [];
    result = dataJson.map(function(item) {
        var newStaff = new Staff (
            item.staffId,
            item.fullName,
            item.email,
            item.passWord,
            item.workingDate,
            item.basicSalary,
            item.position,
            item.workingHours
        );
        return newStaff;
    });
    return result;
}

document.getElementById('btnThemNV').addEventListener('click', function(){
    themNV();
})


// delete staff
function deleteStaff(staffId) {
    var index = findById(staffId);
    if(index != -1) {
        staffList.splice(index,1);
        setStaffList();
        renderStaff(staffList);
    }
}
// UPDATE STAFF: Gồm 2 phần
// Update phần 1: Lấy thông tin show lên form
function getUpdate(staffId) {
    var index = findById(staffId);
    if(index == -1) {
        return alert('Mã nhân viên không tồn tại!');
    }

    var staff = staffList[index];
    // Hiện thông tin nhân viên khi ấn nút sửa
    document.getElementById('tknv').disabled = true;
    document.getElementById('tknv').value = staff.staffId;
    document.getElementById('name').value = staff.fullName;
    document.getElementById('email').value = staff.email;
    document.getElementById('password').value = staff.passWord;
    document.getElementById('datepicker').value = staff.workingDate;
    document.getElementById('luongCB').value = staff.basicSalary;
    document.getElementById('chucvu').value = staff.position;
    document.getElementById('gioLam').value = staff.workingHours;

    clearThongBao();
}

// Update phần 2: Cho người dùng sửa thông tin trên form, ấn nút cập nhật => lưu thay đổi
function updateStaff() {
    var staff = createStaff();

    let isValid = validateForm(staff);
    if(isValid) {
        var index = findById(staff.staffId);
        if(index != -1) {
            staffList[index] = staff;
            setStaffList();
            renderStaff(staffList);
            // clear form
            document.getElementById('tknv').disabled = false;
            document.getElementById('staffForm').reset();
            // close form
            document.getElementById('btnCapNhat').setAttribute('data-dismiss','modal')
        }
    }
}

function ktForm(){
    var temp = document.getElementById('tknv').disabled;
    document.getElementById('datepicker').value = '';
    if(temp) {
        document.getElementById('tknv').disabled = false;
        document.getElementById('staffForm').reset();
    }
    clearThongBao();
}

// search
document.getElementById('searchName').addEventListener('keyup', function() {
    searchStaff();
})
document.getElementById('btnTimNV').addEventListener('click', function() {
    searchStaff();
})
