// 
function kiemTraTrung(idStaff, staffList) {
    let index = staffList.findIndex(function (item) {
        return item.staffId == idStaff;
    })
    
    if(index != -1) {
        document.getElementById('tbTKNV').style.display = 'block';
        document.getElementById('tbTKNV').innerText = '*Mã nhân viên đã tồn tại!';
        return false;
    }
    else {
        document.getElementById('tbTKNV').innerText = '';
        return true;
    }
}

// required: không được để trống
function required(value, idErr) {
    var length = value.length;
    if(length === 0) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Trường này bắt buộc nhập!'
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// kiểm tra độ dài
function kiemTraDoDai(value, idErr, min, max) {
    var length = value.length;
    if(length > max || length < min) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerHTML = `*Độ dài phải từ ${min} đến ${max} kí tự!`
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

//kiểm tra số
function checkNumberId(value) {
    let pattern = /^\d+$/;
    if(!pattern.test(value)) {
        document.getElementById('tbTKNV').style.display = 'block';
        document.getElementById('tbTKNV').innerText = '*Tài khoản nhân viên phải là số!';
        return false;
    }
    else {
        document.getElementById('tbTKNV').innerText = '';
        return true;
    }
}

//check name
function string(value, idErr) {
    let pattern = /^(([a-zA-Z\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*)([a-zA-Z\s\'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*)([a-zA-Z\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]))*$/g;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Tên nhân viên phải là chữ!';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}


// check email
function checkEmail(value, idErr) {
    let pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Email phải đặt theo cú pháp text@gmail.com!';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// check password
function checkPassword(value, idErr) {
    let pattern = /^(?=.*?[A-Z]{1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,}).{6,10}$/;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}


// check date
function checkDate(value, idErr) {
    let pattern = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Ngày phải có định dạng mm/dd/yyyy';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
} 

// check basicSalary and workingHours
function checkNumber(value, idErr, min, max, text) {
    if(value > max || value < min) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerHTML = `${text} ${min} tới ${max}`;
        return false;
    }
    else {
        document.getElementById(idErr).innerHTML = '';
        return true;
    }
}

// check position
function checkPosition(value, idErr) {
    if(value =='Chọn chức vụ') {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Chưa chọn chức vụ!';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// VALIDATION FORM
function validateForm(newStaff) {
    var isValid = true;
    
    isValid = required(newStaff.fullName, "tbTen") && string(newStaff.fullName, 'tbTen');
    isValid &= required(newStaff.email, "tbEmail") && checkEmail(newStaff.email, 'tbEmail');
    isValid &= required(newStaff.passWord, 'tbMatKhau') && checkPassword(newStaff.passWord, 'tbMatKhau');
    isValid &= required(newStaff.workingDate, 'tbNgay') && checkDate(newStaff.workingDate, 'tbNgay');
    isValid &= required(newStaff.basicSalary, 'tbLuongCB') && checkNumber(newStaff.basicSalary, 'tbLuongCB', 1e+6, 20e+6, 'Lương cơ bản từ ');
    isValid &= required(newStaff.workingHours, 'tbGiolam') && checkNumber(newStaff.workingHours, 'tbGiolam', 80, 200, '*Số giờ làm từ ');
    isValid &= checkPosition(newStaff.position, 'tbChucVu');
    
    return isValid;
}

