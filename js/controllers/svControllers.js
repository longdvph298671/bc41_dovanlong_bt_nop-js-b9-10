
function createStaff() {
    var staffId = document.getElementById('tknv').value;
    var fullName = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var passWord = document.getElementById('password').value;
    var workingDate = document.getElementById('datepicker').value;
    var basicSalary = document.getElementById('luongCB').value;
    var position = document.getElementById('chucvu').value;
    var workinghours = document.getElementById('gioLam').value;

    var staff = new Staff (
        staffId,
        fullName,
        email,
        passWord,
        workingDate,
        basicSalary,
        position,
        workinghours
    )
    return staff;
}

function renderStaff(staffList) {
    var contentHTML = '';
    for(var i = 0; i < staffList.length; i++){
        var currentStaff = staffList[i];
        var contentTr = `<tr>
                            <td>${currentStaff.staffId}</td>
                            <td>${currentStaff.fullName}</td>
                            <td>${currentStaff.email}</td>
                            <td>${currentStaff.workingDate}</td>
                            <td>${currentStaff.position}</td>
                            <td>${currentStaff.totalSalary()}</td>
                            <td>${currentStaff.type()}</td>
                            <td>
                                <button onclick="getUpdate('${currentStaff.staffId}')" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
                                <button onclick="deleteStaff('${currentStaff.staffId}')" class="btn btn-danger">Xoá</button>
                            </td>
                        </tr>`
                        contentHTML += contentTr;
    }
    document.getElementById('tableDanhSach').innerHTML = contentHTML;
}

function findById(staffId) {
    for(var i = 0; i < staffList.length; i++){
        if(staffList[i].staffId == staffId) {
            return i;
        }
    }
    return -1;
}

function clearThongBao(){
    document.getElementById('tbTKNV').innerText = '';
    document.getElementById('tbTen').innerText = '';
    document.getElementById('tbEmail').innerText = '';
    document.getElementById('tbMatKhau').innerText = '';
    document.getElementById('tbNgay').innerText = '';
    document.getElementById('tbLuongCB').innerText = '';
    document.getElementById('tbChucVu').innerText = '';
    document.getElementById('tbGiolam').innerText = '';
}

function searchStaff() {
    var keyWord = document.getElementById('searchName').value.toLowerCase();
    var result = [];
    for(var i = 0; i < staffList.length; i++) {
        var staffType = staffList[i].type().toLowerCase();
        if(staffType.search(keyWord) != -1){
            result.push(staffList[i]);
        }
    }
    renderStaff(result);
}
