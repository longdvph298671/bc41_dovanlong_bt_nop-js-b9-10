
//create staff
function Staff(
    staffId,
    fullName,
    email,
    passWord,
    workingDate,
    basicSalary,
    position,
    workingHours,
) {
    this.staffId = staffId;
    this.fullName = fullName;
    this.email = email;
    this.passWord = passWord;
    this.workingDate = workingDate;
    this.basicSalary = basicSalary;
    this.position = position;
    this.workingHours = workingHours;
    this.totalSalary = function () {
        switch(this.position) {
            case 'Sếp':
                return basicSalary * 3;
            case 'Trưởng phòng':
                return basicSalary * 2;
            case 'Nhân viên':
                return basicSalary * 1;
        };
    };
    this.type = function () {
        if(this.workingHours >= 192){
            return 'Nhân viên xuất sắc';
        }
        else if(this.workingHours >= 176){
            return 'Nhân viên giỏi';
        }
        else if(this.workingHours >= 160){
            return 'Nhân viên khá';
        }
        else{
            return 'Nhân viên trung bình';
        };
    };
}